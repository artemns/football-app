<?php

namespace App\Http\Controllers\Football;

use App\Models\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class TeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request -> input();
        $teamName = $data['teamName'];
        $id_group = (int)$data['id_group'];
        $name_group = $data['name_group'];


        if (empty($teamName)){
            return back();
        }

        $teams = DB::table('teams')
            ->where([
                ['id_group', '=', $id_group],
                ['teams.name', '=', $teamName],
            ])->get();

        if (count($teams) <> 0){
            return back()->withInput($data);
        }
        $item = new Team();

        $item->setAttribute('name',$teamName);
        $item->setAttribute('id_group',$id_group);
        $item->save();

        return back()->withInput($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //dd(__METHOD__);
        $groups = DB::table('groups')
            ->where('groups.name', '=', $id)
            ->get()
            ->toArray();

        $group = $groups[0];
        $id_group = $groups[0]->id;


        $teams = DB::table('teams')
            ->where('id_group', '=', $id_group)
            ->get();

        $matches = DB::table('matches')->select('*','matches.id as match_id', 'team1.name as team1_name', 'team2.name as team2_name')
            ->leftJoin('teams as team1', 'matches.id_team1', '=', 'team1.id')
            ->leftJoin('teams as team2', 'matches.id_team2', '=', 'team2.id')
            ->where('matches.id_group', '=', $id_group)
            ->get();


        $exist_score = false;

        $scores_sum = DB::table('matches')->selectRaw('(score_team1+score_team2) as result')
            ->where('matches.id_group', '=', $id_group)
            ->get();

        foreach ($scores_sum as $value){
            if ($value->result > 0){
                $exist_score = true;
                break;
            }
        }
        return view('football.group',compact(['group', 'teams', 'matches', 'exist_score']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Team::destroy($id);
        return back();
    }
}
