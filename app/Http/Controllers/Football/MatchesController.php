<?php

namespace App\Http\Controllers\Football;

use App\Models\Match;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MatchesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request -> input();
        $id_group = (int)$data['id_group'];

        $teams = DB::table('teams')
            ->where('id_group', '=', $id_group)
            ->get();


        for ($i = 0; $i < count($teams); $i++) {
            $team1 = $teams[$i];
            for ($y = $i + 1; $y < count($teams); $y++) {
                $team2 = $teams[$y];

                $count_matches = DB::table('matches')
                    ->where([
                        ['matches.id_team1', '=', $team1->id],
                        ['matches.id_team2', '=', $team2->id],
                    ])
                    ->count();

                if ($count_matches == 0) {
                    $item = new Match();
                    $item->setAttribute('id_group', $id_group);
                    $item->setAttribute('id_team1', $team1->id);
                    $item->setAttribute('id_team2', $team2->id);
                    $item->save();
                }
            }
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $data = $request->input();

        $match = Match::find($data['id_match']);
        if ($match -> exists){
            $match->score_team1 = $data["score_team1"];
            $match->score_team2 = $data["score_team2"];
            $match->save();
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
