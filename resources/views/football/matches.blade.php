<div class="flex-col">
    <form method="POST" action="{{action('Football\MatchesController@store')}}">
        {{ csrf_field() }}

        <input name="id_group" type="hidden" value="{{$group->id}}">

        <div class="flex-between">
            <span>Matches: </span>
            <button type="submit" class="button-container"
                    @if ($exist_score || count($teams) < 2)  style="background-color: rgb(170, 170, 170)"
                    disabled @endif>Generate
            </button>
        </div>
    </form>
    <div class="match-list flex-col">
        <div>
        @foreach($matches as $match)
                <form method="POST" action="{{action('Football\MatchesController@update', $match->match_id)}}">
                    @method('PATCH')
                    {{ csrf_field() }}
                    <input name="id_match" type="hidden" value="{{$match->match_id}}">
                    <div class="match-item flex-between list-item-enter-done">
                        <span class="flex-grow match-team" aria-label="1">
                            {{$match->team1_name}}
                        </span>
                        <div class="flex">
                            <div class="input-container">
                                <input name="score_team1" type="text" autocomplete="off" value="{{$match->score_team1}}">
                            </div>
                            :
                            <div class="input-container">
                                <input name="score_team2" type="text" autocomplete="off" value="{{$match->score_team2}}">
                            </div>
                        </div>
                        <span class="flex-grow match-team">
                            {{$match->team2_name}}
                        </span>
                    </div>
                    <input type="submit" hidden>
                </form>
        @endforeach
        </div>
    </div>
</div>

