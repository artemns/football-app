@extends('layouts.app')

@section('content')
    <div class="app-container">
    <div style="outline: currentcolor none medium;" tabindex="-1" role="group">
        <div class="page groups-list-container">
            <div class="flex"><span>Groups:</span>
                <!--<div class="button-container"><span>New</span></div>-->
                <div class="button-container" @if (count($items) >= 26) style="background-color: rgb(170, 170, 170)" @endif>
                <a @if (count($items) < 26) href="{{ route('create') }}" @endif>New</a>
                </div>
            </div>
            <ul class="flex-col">
                <div>
                    @foreach($items as $item)
                        <li class="group-item"><a href="/group/{{$item->name}}">{{$item->name}}</a>
                            <a href="{{ action('Football\GroupsController@destroy', ['id' => $item->id]) }}"><span class="icon" style="cursor: pointer";>⤫</span></a>
                        </li>
                    @endforeach
                </div>
            </ul>
        </div>
    </div>
    </div>
@endsection
