@extends('layouts.app')

@section('content')
    <div class="app-container">
        <div style="outline: currentcolor none medium;" tabindex="-1" role="group">
            <div class="page groups-list-container">
                <div class="page group-container">
                    <a href="/">
                        <span class="icon" style="cursor: inherit;">⟵</span>
                        <span>Go Back</span>
                    </a>
                    <div class="flex-col">
                        <span>Group: {{$group->name}}</span>
                    </div>

                    @include('football.teams')

                    @include('football.matches')

                </div>
            </div>
        </div>
    </div>
@endsection
