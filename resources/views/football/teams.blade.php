<div class="flex-col">
    <form method="POST" action="{{ action('Football\TeamsController@store')}}">
        {{ csrf_field() }}
        <input name="id_group" type="hidden" value="{{$group->id}}">
        <input name="name_group" type="hidden" value="{{$group->name}}">
        <div class="flex-between new-team-controls">
            <span>Team: </span>
            <div class="input-container">
                <input name="teamName" type="text" autocomplete="off" value="{{ old('teamName') }}">
            </div>
            <button type="submit" class="button-container"
                    @if ($exist_score) style="background-color: rgb(170, 170, 170)" disabled @endif>Add
            </button>
        </div>
    </form>
</div>
<div class="flex-col">
    <span>Teams: </span>
    <div class="flex-col team-list">
        <ol>
            <div>
                @foreach($teams as $team)
                    <li class="team-item">
                        <span class="icon" style="cursor: pointer;"><a
                                href="{{ action('Football\TeamsController@destroy', ['id' => $team->id]) }}">⤫</a></span>
                        <span>{{$team->name}}</span>
                    </li>
                @endforeach
            </div>
        </ol>
    </div>
</div>
