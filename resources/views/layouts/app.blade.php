<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="theme-color" content="#000000"><link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>App</title>
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

</head>
<body>
    <main>
        <div id="root">
            @yield('content')
        <div id="root">
    </main>
</body>
</html>
