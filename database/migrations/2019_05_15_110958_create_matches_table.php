<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_group')->unsigned();
            $table->integer('id_team1')->unsigned();
            $table->integer('score_team1')->unsigned()->default(0);
            $table->integer('id_team2')->unsigned();
            $table->integer('score_team2')->unsigned()->default(0);
            $table->timestamps();

            $table->foreign('id_group')->references('id')->on('groups')->onDelete('cascade');
            $table->foreign('id_team1')->references('id')->on('teams')->onDelete('cascade');
            $table->foreign('id_team2')->references('id')->on('teams')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
