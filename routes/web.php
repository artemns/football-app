<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('', function () {
    return view('welcome');
});*/

Route::group(['namespace' => 'Football'], function () {
    Route::resource('', 'GroupsController');
});

Route::group(['namespace' => 'Football'], function () {
    Route::resource('group', 'TeamsController');
});

Route::group(['namespace' => 'Football'], function () {
    Route::resource('generate', 'MatchesController');
});

Route::get('/{id}/delete', 'Football\GroupsController@destroy');

Route::post('group/', 'Football\TeamsController@store');

Route::get('group/{id}/delete', 'Football\TeamsController@destroy');

Route::post('generate/', 'Football\MatchesController@store');

Route::patch('generate/{id}', 'Football\MatchesController@update');

/*Route::post('group/',function (){
    print_r($_POST);
});*/
